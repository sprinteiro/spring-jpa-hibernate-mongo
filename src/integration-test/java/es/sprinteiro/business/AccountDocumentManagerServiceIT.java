package es.sprinteiro.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.jpa.entities.AccountMock;
import es.sprinteiro.util.UtilDateTime;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-spring-business.xml", 
	"classpath:test-spring-persistence-jpa.xml", "classpath:test-spring-persistence-mongo.xml"})
@Transactional
@ActiveProfiles("default")
public class AccountDocumentManagerServiceIT {
	private Logger log = Logger.getLogger(AccountDocumentManagerServiceIT.class); 
	
	private AccountDocumentManager accountDocumentManagerService;
	
	
	@Autowired
	public void setAccountManagerService(AccountDocumentManager accountDocumentManagerService) {
		this.accountDocumentManagerService = accountDocumentManagerService;
	}
	
	@Test
	public void createReadUpdateDeleteOneAccountDocumentTest() {
		log.info("Begining CRUD...");
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		
		createReadUpdateOneAccountDocumentTestLauncher(expected);
		deleteOneAccountDocumentTestLauncher(expected);
		
		log.info("CRUD succesfully finished!");
	}

	@Test
	public void createReadUpdateDeleteOneAccountWithCustomerDocumentTest() {
		log.info("Begining CRUD...");
		
		Account expected = AccountMock.createAccountWithCustomerWithIdAndExpirityDay();
		
		createReadUpdateOneAccountDocumentTestLauncher(expected);
		
		assertNotNull(expected.getCustomer());
		assertNotNull(expected.getCustomer().getId());
		assertNotNull(expected.getCustomer().getFirstName());
		assertNotNull(expected.getCustomer().getLastName());
		
		deleteOneAccountDocumentTestLauncher(expected);

		log.info("CRUD succesfully finished!");
	}

	
	private List<Account> findAccountDocumentByExpirityDate(final Date date, final String pattern) {
		String expectedDateTime = UtilDateTime.convertDateFormatToString(date, pattern);
		List<Account> accounts = null;
		
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			accounts = accountDocumentManagerService.findAccountDocumentByExpirityDateGreaterThan(dateFormat.parse(expectedDateTime));
		} catch (ParseException e) {
			throw new IllegalArgumentException("Datetime string not correct", e);
		}
		
		log.info("Accounts by " + expectedDateTime);
		log.info(accounts);
		
		return accounts;
	}	
	
	private void createReadUpdateOneAccountDocumentTestLauncher(Account expected) {
		// Create
		Account actual = accountDocumentManagerService.newAccountDocument(expected);
		Long expectedId = actual.getId();
		
		// Read
		actual = accountDocumentManagerService.findAccountDocument(expectedId);
		assertNotNull(actual);
		assertEquals(expectedId, actual.getId());
		String expectedDateTime = UtilDateTime.convertDateFormatToString(expected.getExpirityDate(), "dd-MM-yyyy HH:mm:ss zzz");
		String actualDateTime = UtilDateTime.convertDateFormatToString(actual.getExpirityDate(), "dd-MM-yyyy HH:mm:ss zzz");
		assertEquals(expectedDateTime, actualDateTime);
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(2012, 11, 11);
				
		actual.setExpirityDate(calendar.getTime());
		// Update
		accountDocumentManagerService.modifyAccountDocument(actual);
		actual = accountDocumentManagerService.findAccountDocument(expectedId);
		assertEquals(expected.getId(), actual.getId());
		expectedDateTime = UtilDateTime.convertDateFormatToString(calendar.getTime(), "dd-MM-yyyy HH:mm:ss zzz");
		actualDateTime = UtilDateTime.convertDateFormatToString(actual.getExpirityDate(), "dd-MM-yyyy HH:mm:ss zzz");
		assertEquals(expectedDateTime, actualDateTime);		
	}
	
	private void deleteOneAccountDocumentTestLauncher(Account expected) {
		// Delete
		accountDocumentManagerService.removeAccountDocument(expected.getId());
		assertNull(accountDocumentManagerService.findAccountDocument(expected.getId()));
	}	
	
	@Test
	public void findAccountDocumentAfterCreatedAndLaterRemovedByExpirityDate() {
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		Account actual = accountDocumentManagerService.newAccountDocument(expected);
		assertNotNull(actual);
		
		// Patterns: "dd/MM/yyyy", "dd/MM/yyyy HH:mm, zzz"
		List<Account> actualList = findAccountDocumentByExpirityDate(expected.getExpirityDate(), "dd/MM/yyyy");
		
		assertNotNull(actualList);
		assertTrue(actualList.size() > 0);
		
		accountDocumentManagerService.removeAccountDocument(actual.getId());
		
		assertNull(accountDocumentManagerService.findAccountDocument(actual.getId()));
	}	
	
	@Test
	public void findAccountDocumentAfterCreatedAndLaterRemovedByCustomerFirstAndLastName() {
		Account expected = AccountMock.createAccountWithCustomerWithIdAndExpirityDay();
		Account actual = accountDocumentManagerService.newAccountDocument(expected);
		assertNotNull(actual);
		
		List<Account> actualList = 
				accountDocumentManagerService.findAccountDocumentByCustomer_FirstNameAndCustomer_LastName(
					expected.getCustomer().getFirstName(), expected.getCustomer().getLastName());
		assertNotNull(actualList);
		assertTrue(actualList.size() > 0);
		
		accountDocumentManagerService.removeAccountDocument(actual.getId());		
	}
			
	@Test(expected=RuntimeException.class)
	public void deleteNonExistentAccountDocument() {
		accountDocumentManagerService.removeAccountDocument(-999L);
	}

}
