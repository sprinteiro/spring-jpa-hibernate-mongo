package es.sprinteiro.facade.ws.rest;


import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.jpa.entities.AccountMock;

import static org.junit.Assert.*;


//TODO:
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value={"classpath:test-spring-ws.xml"})
//@TransactionConfiguration(transactionManager="transactionManager")
//@Transactional
@ActiveProfiles("default")
public class GatewayControllerDefaultIT {
	private Logger log = Logger.getLogger(GatewayControllerDefaultIT.class);
	
	private String baseUrl;
	
	private RestTemplate restTemplate;

	@Value("#{myProps[baseurl]}")
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl + "/gate";
	}
	
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

//	@Test
	public void lookUpTest() {
		log.info("Testing Lookup!");
		String result = restTemplate.getForObject(baseUrl + "/lookup", String.class);
		assertNotNull(result);
		log.info("Testing Lookup succesfully! (" + result + ")");
	}
	
	@Test
	public void newAccount() {
		Account expected = AccountMock.createAccountWithoutIdAndExpirityDay();
		log.info("New Account POST... (" + expected + ")");
		org.springframework.http.ResponseEntity<String> response = restTemplate.postForEntity(baseUrl, expected, String.class);
		assertNotNull(response);
		log.info("Operation result=" + response);
		@SuppressWarnings("unused")
		String URI[] = response.getHeaders().getLocation().toString().split("/");
		
		log.info("New Account POST successfully finished!");
	}
	
	@Test
	public void newAccountAndCustomer() {
		Account expected = AccountMock.createAccountWithCustomerWithoutIdAndExpirityDay();

		log.info("New Account and Customer POST... (" + expected + ")");
		org.springframework.http.ResponseEntity<String> response = restTemplate.postForEntity(baseUrl, expected, String.class);
		assertNotNull(response);
		log.info("Operation result=" + response);
		@SuppressWarnings("unused")
		String URI[] = response.getHeaders().getLocation().toString().split("/");
		
		log.info("New Account and Customer POST successfully finished!");
	}
}
