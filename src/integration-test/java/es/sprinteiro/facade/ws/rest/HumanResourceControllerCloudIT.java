package es.sprinteiro.facade.ws.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import es.sprinteiro.persistence.document.Person;
import es.sprinteiro.persistence.document.PersonMock;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value={"classpath:test-spring-ws.xml"})
@ActiveProfiles("cloud")
public class HumanResourceControllerCloudIT {
	private final Logger log = Logger.getLogger(HumanResourceControllerCloudIT.class);
	
	private String baseUrl;
	
	private RestTemplate restTemplate;

	@Value("#{myProps[baseurl]}")
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl + "/hr";
	}
	
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

//	@Test
	public void lookUpTest() {
		log.info("Testing Lookup!");
		String result = restTemplate.getForObject(baseUrl + "/lookup", String.class);
		assertNotNull(result);
		log.info("Testing Lookup succesfully! (" + result + ")");
	}
	
	@Test 
	public void createReadUpdateDelete() {
		log.info("Begining CRUD...");
		
		Person expected = PersonMock.createPersonWithoutId();
		// Create
		newPerson(expected);
		
		// Read
		Person actual = getPerson(expected);
			
		// Update
		actual.setName(!"Rene".equals(actual.getName()) ? "Rene" : "Michael");
		actual.setAge(actual.getAge() != 31 ? 31 : 25);
		restTemplate.put(baseUrl, actual);
		
		actual = getPerson(actual);
		
		// Delete
		restTemplate.delete(baseUrl + "/person/{id}", actual.getId());
		assertNull(restTemplate.getForObject(baseUrl + "/person/{id}", Person.class, expected.getId()));
		
		log.info("CRUD successfully finished!");
	}
	
	private void newPerson(Person person) {
		log.info("New Person POST... (" + person + ")");
		org.springframework.http.ResponseEntity<String> response = 
			restTemplate.postForEntity(baseUrl, person, String.class);
		
		assertNotNull(response);
		log.info("Operation result=" + response);
		String URI[] = response.getHeaders().getLocation().toString().split("/");
		assertTrue(URI.length == 3);
		person.setId(URI[2]);
	}
	
	private Person getPerson(Person expected) {
		String url = baseUrl + "/person/{id}";
		Person actualResponse = 
				restTemplate.getForObject(url, Person.class, expected.getId());
		
		assertNotNull(actualResponse);
		assertNotNull(actualResponse.getName());
		assertNotNull(actualResponse.getAge());
		assertEquals(expected.getName(), actualResponse.getName());
		assertEquals(expected.getAge(), actualResponse.getAge());	
		
		return actualResponse;
	}
	
	@Test
	public void getTwoPersonsAfterCreatedAndLaterRemovedByNameAndAge() {
		final String url = baseUrl +  "/person/{name}/{age}";
		final String nameToSearch = "Bruce";
		final int ageToSearch = 35;
		
		Person[] personArray = { 
			new Person(nameToSearch, ageToSearch), new Person(nameToSearch, ageToSearch) 
		};
		
		for (Person person : personArray) {
			newPerson(person);
		}
		
		@SuppressWarnings("unchecked")
		List<Person> actualResponse = restTemplate.getForObject(url, List.class, nameToSearch, ageToSearch);
		log.info("Persons -->" + actualResponse);
		
		for (Person person : personArray) {
			restTemplate.delete(baseUrl + "/person/{id}", person.getId());
		}
		
		assertTrue(restTemplate.getForObject(url, List.class, nameToSearch, ageToSearch).isEmpty());
	}
}
