package es.sprinteiro.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.repository.mongo.AccountRepository;


@Service
public class AccountDocumentManagerService implements AccountDocumentManager {
	
	private AccountRepository accountRepositoryMongo;

	
	@Autowired
	public AccountDocumentManagerService(AccountRepository accountRepositoryMongo) {
		  if (accountRepositoryMongo == null) {
			    throw new IllegalArgumentException("AccountRepositoryMongo cannot be null");
			  }
		
		this.accountRepositoryMongo = accountRepositoryMongo;
	}
	
	public AccountRepository getAccountRepository() {
		return accountRepositoryMongo;
	}
	
	@Override
	public Account newAccountDocument(Account expected) {
		return accountRepositoryMongo.save(expected);
	}

	@Override
	public Account findAccountDocument(Long id) {
		return accountRepositoryMongo.findOneById(id);
	}

	@Override
	public Account modifyAccountDocument(Account account) {
		return accountRepositoryMongo.save(account);
	}

	@Override
	public void removeAccountDocument(Long id) {
		Account persisted = accountRepositoryMongo.findOneById(id);
		
		if (persisted == null) {
			// TODO: The account doesn't exist, throw a kind of exception
			throw new RuntimeException("The account doesn't exist (id= + " +  id + ")");
		}
		
		accountRepositoryMongo.delete(persisted);
	}

	@Override
	public List<Account> findAccountDocumentByExpirityDateGreaterThan(Date date) {
		return accountRepositoryMongo.findByExpirityDateGreaterThan(date);
	}

	@Override
	public List<Account> findAccountDocumentByCustomer_FirstNameAndCustomer_LastName(
			String firstName, String lastName) {
		return accountRepositoryMongo.findByCustomer_FirstNameAndCustomer_LastName(firstName, lastName);
	}

}
