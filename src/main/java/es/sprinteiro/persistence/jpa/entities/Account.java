package es.sprinteiro.persistence.jpa.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Account {

	private Long id;
	
	private Customer customer;
	
	private Date expirityDate;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE}, optional=true)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getExpirityDate() {
//		return (Date) expirityDate.clone();
		return expirityDate;
	}

	public void setExpirityDate(Date expirityDate) {
//		this.expirityDate = (Date) expirityDate.clone();
		this.expirityDate = expirityDate;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Account [id=").append(getId())
		  .append(", expirityDate=").append(getExpirityDate())
		  .append(", customer=").append(getCustomer())
		  .append(']');
		
		return sb.toString();
	}
	
}
