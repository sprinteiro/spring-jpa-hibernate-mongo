package es.sprinteiro.persistence.repository.mongo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import es.sprinteiro.persistence.document.Person;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;


//@Component
// Using @Repository instead of @Component because @Repository translate platform-data mongo 
// access exception into generic Spring's data access exception
@Repository
public class PersonRepositoryMongo implements PersonRepository {
	private MongoOperations mongoTemplate;
	
	
	@Autowired
	public PersonRepositoryMongo(MongoOperations mongoTemplate) {
		if (mongoTemplate == null) {
			throw new IllegalArgumentException("MongoTemplate cannot be null.");
		}
		
		this.mongoTemplate = mongoTemplate;
	}

	public MongoOperations getMongoTemplate() {
		return mongoTemplate;
	}
	
	@Override
	public Person save(Person person) {
		mongoTemplate.save(person);
		
		return person;
	}
	
	@Override
	public Person findOneById(String id) {
		return mongoTemplate.findById(id, Person.class);
	}
	
	@Override
	public void delete(Person person) {
		mongoTemplate.remove(person);
	}

	@Override
	public List<Person> findByNameAndAge(String name, int age) {
		// TODO Auto-generated method stub
		return mongoTemplate.find(
			query(where("name").is(name).and("age").in(age)), Person.class);
	}
}
