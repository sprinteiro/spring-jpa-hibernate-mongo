package es.sprinteiro.business;



import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;

import com.rits.cloning.Cloner;

import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.jpa.entities.AccountMock;
import es.sprinteiro.persistence.repository.mongo.AccountRepository;


public class AccountDocumentManagerServiceTest {
	private Logger log = Logger.getLogger(AccountDocumentManagerServiceTest.class);
	
	private AccountDocumentManager accountDocumentManagerService;
	
	private AccountRepository accountRepositoryMongoMock;
	
	private Cloner cloner;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception { 
	}
	
	@Before
	public void setUp() throws Exception { 
		accountRepositoryMongoMock = mock(AccountRepository.class);
		accountDocumentManagerService = new AccountDocumentManagerService(accountRepositoryMongoMock);
		cloner = new Cloner();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception { }
	
	@After
	public void tearDown() throws Exception {
		accountRepositoryMongoMock = null;
		cloner = null;
	}
	
	@Autowired
	public void setAccountDocumentManagerService(AccountDocumentManager accountDocumentManagerService) {
		this.accountDocumentManagerService = accountDocumentManagerService;
	}
	
	@Test
	public void shoudlCreateOneNewAccountDocument() {
		log.info("Begining shoudlCreateOneNewAccountTest...");
		final long id = 111111;
		Account expected = AccountMock.createAccountWithoutIdAndExpirityDay();
		
		when(accountRepositoryMongoMock.save(expected)).thenAnswer(new Answer<Account>() {
			@Override
			public Account answer(InvocationOnMock invocation) throws Throwable {
				Account account = cloner.deepClone((Account) invocation.getArguments()[0]);
				account.setId(id);
				
				return account;
			}
		});
		
		Account actual = accountDocumentManagerService.newAccountDocument(expected);
		
		verify(accountRepositoryMongoMock).save(expected);
		
		assertNotNull(actual);
		assertNotNull(actual.getId());
		assertEquals(Long.valueOf(id), actual.getId());
		assertEquals(expected.getExpirityDate(), actual.getExpirityDate());
		
		log.info("shoudlCreateOneNewAccountTest succesfully finished!");
	}
	
	@Test
	public void shouldModifyOneExistingAccountDocument() {
		log.info("Begining shouldModifyOneExistingAccountDocument...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		
		when(accountRepositoryMongoMock.save(expected)).thenReturn(expected);
		
		Account actual = accountDocumentManagerService.modifyAccountDocument(expected);
		
		verify(accountRepositoryMongoMock).save(expected);
		
		assertNotNull(actual);
		assertNotNull(actual.getId());
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getExpirityDate(), actual.getExpirityDate());
		
		log.info("shouldModifyOneExistingAccountDocument succesfully finished!");
	}
	
	@Test
	public void shouldRemoveOneExistingAccountDocument() {
		log.info("Begining shouldRemoveOneExistingAccountDocument...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		when(accountRepositoryMongoMock.findOneById(expected.getId())).thenReturn(expected);
		
		accountDocumentManagerService.removeAccountDocument(expected.getId());
		
		verify(accountRepositoryMongoMock).delete(expected);
		
		log.info("shouldRemoveOneExistingAccountDocument succesfully finished!");
	}
	
	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionRemoveNotExistingAccountDocument() {
		log.info("Begining shouldThrowExceptionRemoveNotExistingAccountDocument...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		when(accountRepositoryMongoMock.findOneById(expected.getId())).thenReturn(null);
		
		accountDocumentManagerService.removeAccountDocument(expected.getId());
		
		verify(accountRepositoryMongoMock).delete(expected);
		
		log.info("shouldThrowExceptionRemoveNotExistingAccountDocument succesfully finished!");
	}
	
	@Test
	public void shouldFindOneExistingAccountDocumentById() {
		log.info("Begining shouldFindOneExistingAccountDocumentById...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		
		when(accountRepositoryMongoMock.findOneById(expected.getId())).thenReturn(expected);
		
		Account actual = accountDocumentManagerService.findAccountDocument(expected.getId());
		
		assertNotNull(actual);
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getExpirityDate(), actual.getExpirityDate());
		
		log.info("shouldFindOneExistingAccountDocumentById succesfully finished!");
	}
	
	@Test
	public void shouldFindOneAccountDocumentByExpirityDateGreaterThan() {
		log.info("Begining shouldFindOneAccountDocumentByExpirityDateGreaterThan...");
		
		final long MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000;
		final int NUMBER_OF_DAYS = 3;
		final Date CURRENT_DATE = new Date();
		
		List<Account> expectedAccounts = new ArrayList<Account>(2);
		Account account = AccountMock.createAccountWithIdAndExpirityDay();
		account.setExpirityDate(new Date (CURRENT_DATE.getTime() + (MILLISECONDS_PER_DAY * NUMBER_OF_DAYS)));
		expectedAccounts.add(account);
		
		account = AccountMock.createAccountWithIdAndExpirityDay();
		account.setExpirityDate(new Date (CURRENT_DATE.getTime() + (MILLISECONDS_PER_DAY * (NUMBER_OF_DAYS + 1))));
		expectedAccounts.add(account);
		
		when(accountRepositoryMongoMock.findByExpirityDateGreaterThan(CURRENT_DATE)).thenReturn(expectedAccounts);
		
		List<Account> actualAccounts = accountDocumentManagerService.findAccountDocumentByExpirityDateGreaterThan(CURRENT_DATE);
		
		verify(accountRepositoryMongoMock).findByExpirityDateGreaterThan(CURRENT_DATE);
		
		assertNotNull(actualAccounts);
		assertTrue(expectedAccounts.size() == actualAccounts.size());
		for (Account acc : actualAccounts) {
			assertTrue (acc.getExpirityDate().after(CURRENT_DATE));
		}
	
		log.info("Begining shouldFindOneAccountDocumentByExpirityDateGreaterThan  succesfully finished!");
	}
	
	@Test
	public void shouldFindAccountDocumentByCustomer_FirstNameAndCustomer_LastName() {
		log.info("Begining shouldFindAccountDocumentByCustomer_FirstNameAndCustomer_LastName...");
		
		Account accountOne = AccountMock.createAccountWithCustomerWithIdAndExpirityDay();
		Account accountTwo = cloner.deepClone(accountOne);
		accountTwo.setId(accountOne.getId() != 111111 ? 111111L : 222222L);
		List<Account> expectedAccounts = new ArrayList<Account>(2);
		expectedAccounts.add(accountOne); expectedAccounts.add(accountTwo);

		final String firstNameQuery = accountOne.getCustomer().getFirstName();
		final String lastNameQuery = accountOne.getCustomer().getLastName();
		
		when(accountRepositoryMongoMock.findByCustomer_FirstNameAndCustomer_LastName(firstNameQuery, lastNameQuery))
		.thenReturn(expectedAccounts);
		
		List<Account> actualAccounts = accountDocumentManagerService.findAccountDocumentByCustomer_FirstNameAndCustomer_LastName(firstNameQuery, lastNameQuery);
		
		verify(accountRepositoryMongoMock).findByCustomer_FirstNameAndCustomer_LastName(firstNameQuery, lastNameQuery);
		
		assertNotNull(actualAccounts);
		assertTrue(expectedAccounts.size() == actualAccounts.size());
		
		log.info("shouldFindAccountDocumentByCustomer_FirstNameAndCustomer_LastName succesfully finished!");
	}
}
