package es.sprinteiro.business;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rits.cloning.Cloner;

import es.sprinteiro.persistence.jpa.entities.Account;
import es.sprinteiro.persistence.jpa.entities.AccountMock;
import es.sprinteiro.persistence.repository.jpa.AccountRepositoryJpa;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;



public class AccountManagerServiceTest {
	private final Logger log = Logger.getLogger(AccountManagerServiceTest.class);

	private AccountManagerService accountManagerService;
	
	private AccountRepositoryJpa accountRepositoryMock;
	
	private Cloner cloner;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception { }
	
	@Before
	public void setUp() throws Exception {
		accountRepositoryMock = mock(AccountRepositoryJpa.class);
		accountManagerService = new AccountManagerService(accountRepositoryMock);
		cloner = new Cloner();
	}
	
	@After
	public void tearDown() throws Exception {
		accountManagerService = null;
		accountRepositoryMock = null;
		cloner = null;
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception { }
	

	@Test
	public void shoudlCreateOneNewAccount() {
		log.info("Begining shoudlCreateOneNewAccountTest...");
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		
		when(accountRepositoryMock.save(expected)).thenReturn(expected);
		
		Account actual = accountManagerService.newAccount(expected);
		
		verify(accountRepositoryMock).save(expected);
		
		assertNotNull(actual);
		
		log.info("shoudlCreateOneNewAccountTest succesfully finished!");
	}
	
	@Test
	public void shouldModifyOneExistingAccount() {
		log.info("Begining shouldModifyOneExistingAccount...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		
		when(accountRepositoryMock.save(expected)).thenReturn(expected);
		
		Account actual = accountManagerService.modifyAccount(expected);
		
		verify(accountRepositoryMock).save(expected);
		
		assertNotNull(actual);
		
		log.info("shouldModifyOneExistingAccount succesfully finished!");
	}
	
	@Test
	public void shouldRemoveOneExistingAccount() {
		log.info("Begining shouldRemoveOneExistingAccount...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		when(accountRepositoryMock.findOne(expected.getId())).thenReturn(expected);
		
		accountManagerService.removeAccount(expected.getId());
		
		verify(accountRepositoryMock).delete(expected);
		
		log.info("shouldRemoveOneExistingAccount succesfully finished!");
	}
	
	@Test(expected = RuntimeException.class)
	public void shouldThrowExceptionRemoveNotExistingAccount() {
		log.info("Begining shouldThrowExceptionRemoveNotExistingAccount...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		when(accountRepositoryMock.findOne(expected.getId())).thenReturn(null);
		
		accountManagerService.removeAccount(expected.getId());
		
		verify(accountRepositoryMock).delete(expected);
		
		log.info("shouldThrowExceptionRemoveNotExistingAccount succesfully finished!");
	}
	
	@Test
	public void shouldFindOneExistingAccountById() {
		log.info("Begining shouldFindOneExistingAccountById...");
		
		Account expected = AccountMock.createAccountWithIdAndExpirityDay();
		
		when(accountRepositoryMock.findOne(expected.getId())).thenReturn(expected);
		
		Account actual = accountManagerService.findAccount(expected.getId());
		
		assertNotNull(actual);
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getExpirityDate(), actual.getExpirityDate());
		
		log.info("shouldFindOneExistingAccountById succesfully finished!");
	}
	
	@Test
	public void shouldFindOneAccountByExpirityDateGreaterThan() {
		//TODO
		log.info("Begining shouldFindOneAccountByExpirityDateGreaterThan...");
		
		final long MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000;
		final int NUMBER_OF_DAYS = 3;
		final Date CURRENT_DATE = new Date();
		
		List<Account> expectedAccounts = new ArrayList<Account>(2);
		Account account = AccountMock.createAccountWithIdAndExpirityDay();
		account.setExpirityDate(new Date (CURRENT_DATE.getTime() + (MILLISECONDS_PER_DAY * NUMBER_OF_DAYS)));
		expectedAccounts.add(account);
		
		account = AccountMock.createAccountWithIdAndExpirityDay();
		account.setExpirityDate(new Date (CURRENT_DATE.getTime() + (MILLISECONDS_PER_DAY * (NUMBER_OF_DAYS + 1))));
		expectedAccounts.add(account);
		
		when(accountRepositoryMock.findByExpirityDateGreaterThan(CURRENT_DATE)).thenReturn(expectedAccounts);
		
		List<Account> actualAccounts = accountManagerService.findAccountByExpirityDateGreaterThan(CURRENT_DATE);
		
		verify(accountRepositoryMock).findByExpirityDateGreaterThan(CURRENT_DATE);
		
		assertNotNull(actualAccounts);
		assertTrue(expectedAccounts.size() == actualAccounts.size());
		for (Account acc : actualAccounts) {
			assertTrue (acc.getExpirityDate().after(CURRENT_DATE));
		}
	
		log.info("Begining shouldFindOneAccountByExpirityDateGreaterThan  succesfully finished!");
	}
	
	@Test
	public void shouldFindAccountByCustomer_FirstNameAndCustomer_LastName() {
		log.info("Begining shouldFindAccountByCustomer_FirstNameAndCustomer_LastName...");
		
		Account accountOne = AccountMock.createAccountWithCustomerWithIdAndExpirityDay();
		Account accountTwo = cloner.deepClone(accountOne);
		accountTwo.setId(accountOne.getId() != 111111 ? 111111L : 222222L);
		List<Account> expectedAccounts = new ArrayList<Account>(2);
		expectedAccounts.add(accountOne); expectedAccounts.add(accountTwo);

		final String firstNameQuery = accountOne.getCustomer().getFirstName();
		final String lastNameQuery = accountOne.getCustomer().getLastName();
		
		when(accountRepositoryMock
		.findAccountByCustomer_FirstNameAndCustomer_LastName(firstNameQuery, lastNameQuery))
		.thenReturn(expectedAccounts);
		
		List<Account> actualAccounts = accountManagerService.findAccountByCustomer_FirstNameAndCustomer_LastName(firstNameQuery, lastNameQuery);
		
		verify(accountRepositoryMock).findAccountByCustomer_FirstNameAndCustomer_LastName(firstNameQuery, lastNameQuery);
		
		assertNotNull(actualAccounts);
		assertTrue(expectedAccounts.size() == actualAccounts.size());
		
		log.info("shouldFindAccountByCustomer_FirstNameAndCustomer_LastName succesfully finished!");
	}
	
	
}
