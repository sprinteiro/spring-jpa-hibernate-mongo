package es.sprinteiro.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.rits.cloning.Cloner;

import es.sprinteiro.persistence.document.Person;
import es.sprinteiro.persistence.document.PersonMock;
import es.sprinteiro.persistence.repository.mongo.PersonRepository;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class HumanResourceManagerServiceTest {
	private final Logger log = Logger.getLogger(HumanResourceManagerServiceTest.class); 
	
	private PersonRepository personRepositoryMock;
	
	private HumanResourceManagerService humanResourceManager;

	private Cloner cloner;
	
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception { }
	
	@Before
	public void setUp() throws Exception {
		cloner = new Cloner();
		personRepositoryMock = mock(PersonRepository.class);
		humanResourceManager = new HumanResourceManagerService(personRepositoryMock);
	}
	
	@After
	public void tearDown() throws Exception{
		cloner = null;
		personRepositoryMock = null;
		humanResourceManager = null;
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception { }
	
	@Test
	public void shoudlCreateOneNewAccountTest() {
		log.info("Begining shoudlCreateOneNewAccountTest...");
		
		final Person expected = PersonMock.createPersonWithoutId();

		// Asking for an account (not verified next, only stubbing)
		when(personRepositoryMock.save(expected))
		.thenAnswer(new Answer<Person>() {
			@Override
			public Person answer(InvocationOnMock invocation) throws Throwable {
				Person person = cloner.deepClone((Person) invocation.getArguments()[0]);
				person.setId("AABB2102");
				
				return person;
			}
		});
		
		// Launching the test
		Person actual = humanResourceManager.newPerson(expected);
		
		assertNotNull(actual);
		assertNotNull(actual.getId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getAge(), actual.getAge());
		
		log.info("shoudlCreateOneNewAccountTest succesfully finished!");
	}
	
	@Test
	public void shouldModifyOneExistingAccountTest() {
		log.info("Begining shouldModifyOneExistingAccountTest...");
		
		Person expected = PersonMock.createPersonWithId();
		
		// Asking to get an existing account (not verified, just exists)
		when(personRepositoryMock.findOneById(expected.getId())).thenReturn(expected);
		
		expected.setName("Jack".equals(expected.getName()) ? "Maria" : "Jack");
		expected.setAge(expected.getAge() == 20 ? 31 : 20);
		// Asking about modifying the existing account
		when(personRepositoryMock.save(expected)).thenReturn(expected);
		
		// Launching the test
		humanResourceManager.modifyPerson(expected);
		
		// Telling about modifying the existing account (verification)
		verify(personRepositoryMock).save(expected);
		
		log.info("shouldModifyOneExistingAccountTest succesfully finished!");		
	}
	
	@Test
	public void shouldRemoveOneExistingAccount() {
		log.info("Begining shouldRemoveOneExistingAccount...");
		
		Person expected = PersonMock.createPersonWithoutId();
		// Asking to get an existing account (not verified, just exists)
		when(personRepositoryMock.findOneById(expected.getId())).thenReturn(expected);
		
		doNothing().when(personRepositoryMock).delete(expected);
		
		// Launching the test
		humanResourceManager.removePerson(expected.getId());
		
		// Telling about deleting the existing account (verification)
		verify(personRepositoryMock).delete(expected);
		
		log.info("shouldRemoveOneExistingAccount succesfully finished!");
	}

	@Test
	public void shouldFindPersonsByNameAndAge() {
		log.info("Begining shouldFindPersonsByNameAndAge...");
		String nameQuery = "Kuvuni"; int ageQuery = 22;
		
		Person personOne = new Person (nameQuery, ageQuery);
		personOne.setId("AABB01");
		Person personTwo = new Person (nameQuery, ageQuery);
		personTwo.setId("AABB02");

		List<Person> expectedPersonsMock = new ArrayList<Person>(2);
		expectedPersonsMock.add(personOne);
		expectedPersonsMock.add(personTwo);
		
		// Asking to get an existing accounts by name and age (not verified, just exists)
		when(personRepositoryMock.findByNameAndAge(nameQuery, ageQuery)).thenReturn(expectedPersonsMock);
		
		List<Person> actualPersons = humanResourceManager.findPersonsByNameAndAge(nameQuery, ageQuery);
		
		assertNotNull(actualPersons);
		assertTrue(actualPersons.size() == 2);
		Set<String> ids = new HashSet<String>(Arrays.asList("AABB01", "AABB02"));
		for (Person person : actualPersons) {
			assertEquals(nameQuery, person.getName());
			assertEquals(ageQuery, person.getAge());
			assertTrue(ids.contains(person.getId()));
		}
		
		log.info("shouldFindPersonsByNameAndAge succesfully finished!");
	}
	
}
