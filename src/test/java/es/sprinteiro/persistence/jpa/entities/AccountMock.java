package es.sprinteiro.persistence.jpa.entities;

import java.util.Date;

import es.sprinteiro.util.UtilRandomNumber;

public class AccountMock {

	public static Account createAccountWithoutIdAndExpirityDay() {
		Account account = new Account();
		Date currentDate = new Date();
		account.setExpirityDate(new Date (currentDate.getTime() + (2 * 24 * 60 * 60 * 1000)));
		
		return account;
	}
	
	public static Account createAccountWithIdAndExpirityDay() {
		Account account = createAccountWithoutIdAndExpirityDay();
		account.setId((long) UtilRandomNumber.getRandom(1, 1000));
		
		return account;
	}

	public static Account createAccountWithCustomerWithoutIdAndExpirityDay() {
		Account account = createAccountWithoutIdAndExpirityDay();
		account.setCustomer(CustomerMock.createCustomerWithoutId());
		
		return account;
	}
	
	public static Account createAccountWithCustomerWithIdAndExpirityDay() {
		Account account = createAccountWithoutIdAndExpirityDay();
		account.setId((long) UtilRandomNumber.getRandom(1, 1000));
		account.setCustomer(CustomerMock.createCustomerWithId());
		
		return account;
	}
	
}
