package es.sprinteiro.persistence.repository.mongo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;

import es.sprinteiro.persistence.document.Person;
import es.sprinteiro.persistence.document.PersonMock;

import static org.mockito.Mockito.*;


public class PersonRepositoryMongoTest {
	@SuppressWarnings("unused")
	private final Logger log = Logger.getLogger(PersonRepositoryMongoTest.class);
	
	private PersonRepositoryMongo personRepositoryMongo;
	
	private MongoOperations mongoTemplateMock;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception { }
	
	@Before
	public void setUp() throws Exception {
		mongoTemplateMock = mock(MongoOperations.class);
		personRepositoryMongo = new PersonRepositoryMongo(mongoTemplateMock);
	}
	
	@Test
	public void shouldCreateOnePerson() {
		final Person expected = PersonMock.createPersonWithoutId();
		
		doNothing().when(mongoTemplateMock).save(expected);
		when(personRepositoryMongo.save(expected)).thenAnswer(new Answer<Person>() {
			@Override
			public Person answer(InvocationOnMock invocation) throws Throwable {
				expected.setId("AABBCC");
				return null;
			}
		});
		
		Person actual = personRepositoryMongo.save(expected);
		
		verify(mongoTemplateMock).save(expected);
		
		assertNotNull(actual);
		assertNotNull(expected.getId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getAge(), actual.getAge());
	}
	
	@Test
	public void shouldModifyOnePerson() {
		final Person expected = PersonMock.createPersonWithId();
		
		doNothing().when(mongoTemplateMock).save(expected);
		
		Person actual = personRepositoryMongo.save(expected);
		
		verify(mongoTemplateMock).save(expected);
		
		assertNotNull(expected.getId());
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getAge(), actual.getAge());
	}
	
	@Test
	public void shouldRemoveOnePersonById() {
		Person person = PersonMock.createPersonWithId();
		
		doNothing().when(mongoTemplateMock).remove(person);
		
		personRepositoryMongo.delete(person);
		
		verify(mongoTemplateMock).remove(person);
	}
	
	@Test(expected = RuntimeException.class)
	public void shouldThoughDataAccessExceptionWhenRemovingOnePersonWithoutId() {
		Person person = PersonMock.createPersonWithoutId();
		
		doThrow(RuntimeException.class).when(mongoTemplateMock).remove(person);
		
		personRepositoryMongo.delete(person);
	}
	
	@Test
	public void shouldFindOnePersonById() {
		Person expected = PersonMock.createPersonWithId();
		
		when(mongoTemplateMock.findById(expected.getId(), Person.class)).thenReturn(expected);
		
		Person actual = personRepositoryMongo.findOneById(expected.getId());
		
		verify(mongoTemplateMock).findById(expected.getId(), Person.class);
		
		assertNotNull(actual);
		assertEquals(expected.getId(), actual.getId());
	}
	
//	@Test
	public void shouldFindPersonsByNameAndAge() {
		// TODO: It doesn't work
		Person expected = PersonMock.createPersonWithId();
		List<Person> expectedPersons = new ArrayList<Person>();
		expectedPersons.add(expected);
		
		doReturn(expectedPersons).when(mongoTemplateMock).find(mock(Query.class), List.class);
		
		List<Person> actual = personRepositoryMongo.findByNameAndAge(expected.getName(), expected.getAge());
		
		verify(mongoTemplateMock).find(mock(Query.class), List.class);
		
		assertNotNull(actual);
		assertTrue(actual.size() == 1);
	}
}
