package es.sprinteiro.util;

import org.junit.Assert;
import org.junit.Test;

public class UtilRandomNumberTest {
	@Test
	public void getRandom() {
		int min = 1;
		int max = 2012;
		
		Assert.assertTrue(UtilRandomNumber.getRandom(min, max) > 0);
		Assert.assertTrue(UtilRandomNumber.getRandom(min, max) <= 2012);
	}
}
